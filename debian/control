Source: libcvd
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Picca Frédéric-Emmanuel <picca@debian.org>, Roland Mas <lolando@debian.org>
Build-Depends: debhelper-compat (= 13), libtoon-dev,
 libjpeg-dev, libpng-dev, libtiff-dev, liblapack-dev, libv4l-dev,
 mesa-common-dev, libgl1-mesa-dev, libglu1-mesa-dev, mesa-utils,
 freeglut3-dev, ffmpeg, libffms2-dev, libavcodec-dev, libavdevice-dev,
 libavfilter-dev, libavformat-dev, libdc1394-dev
Standards-Version: 4.5.1
Section: libs
Homepage: http://www.edwardrosten.com/cvd/
Vcs-Browser: https://salsa.debian.org/science-team/libcvd
Vcs-Git: https://salsa.debian.org/science-team/libcvd.git
Rules-Requires-Root: no

Package: libcvd-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcvd2 (= ${binary:Version}), ${misc:Depends}
Description: C++ library for computer vision, image, and video processing - devel files
 libCVD is a very portable and high performance C++ library for
 computer vision, image, and video processing. The emphasis is on
 providing simple and efficient image and video handling and high
 quality implementations of common low-level image processing
 function. The library is designed in a loosely-coupled manner, so
 that parts can be used easily in isolation if the whole library is
 not required. The video grabbing module provides a simple, uniform
 interface for videos from a variety of sources (live and recorded)
 and allows easy access to the raw pixel data. Likewise, the image
 loading/saving module provides simple, uniform interfaces for loading
 and saving images from bitmaps to 64 bit per channel RGBA images. The
 image processing routines can be applied easily to images and video,
 and accelerated versions exist for platforms supporting SSE.
 .
 This package contains the development files.

Package: libcvd2
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C++ library for computer vision, image, and video processing - shared library
 libCVD is a very portable and high performance C++ library for
 computer vision, image, and video processing. The emphasis is on
 providing simple and efficient image and video handling and high
 quality implementations of common low-level image processing
 function. The library is designed in a loosely-coupled manner, so
 that parts can be used easily in isolation if the whole library is
 not required. The video grabbing module provides a simple, uniform
 interface for videos from a variety of sources (live and recorded)
 and allows easy access to the raw pixel data. Likewise, the image
 loading/saving module provides simple, uniform interfaces for loading
 and saving images from bitmaps to 64 bit per channel RGBA images. The
 image processing routines can be applied easily to images and video,
 and accelerated versions exist for platforms supporting SSE.
 .
 This package contains the shared library.

Package: libcvd-tools
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C++ library for computer vision, image, and video processing - tools
 libCVD is a very portable and high performance C++ library for
 computer vision, image, and video processing. The emphasis is on
 providing simple and efficient image and video handling and high
 quality implementations of common low-level image processing
 function. The library is designed in a loosely-coupled manner, so
 that parts can be used easily in isolation if the whole library is
 not required. The video grabbing module provides a simple, uniform
 interface for videos from a variety of sources (live and recorded)
 and allows easy access to the raw pixel data. Likewise, the image
 loading/saving module provides simple, uniform interfaces for loading
 and saving images from bitmaps to 64 bit per channel RGBA images. The
 image processing routines can be applied easily to images and video,
 and accelerated versions exist for platforms supporting SSE.
 .
 This package contains tools and utilities using libcvd.
